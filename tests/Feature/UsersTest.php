<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    public function testWelcom()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testLogin()
    {
        $user = User::factory()->create([
            'password' => bcrypt('testing123'),
        ]);
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'testing123',
        ]);

        $response->assertRedirect('/home');

        $this->assertAuthenticatedAs($user);
        // $this->testLogout();
    }
    // function testLogout($user)
    // {
    //     $response = $this->post('/logout', [
    //         'id' => $user->id,
    //     ]);
    //
    //     $response->assertRedirect('/');
    //
    //     $this->assertAuthenticatedAs(null);
    // }
    // public function testRegister()
    // {
    //     // $user = User::factory()->make([
    //     //     'name' => $this->faker->name,
    //     //     'email' => $this->faker->unique()->safeEmail,
    //     //     'password' => bcrypt('testing123'),
    //     // ])->toArray();
    //
    //     $response = $this->post('/register', [
    //         'name' => $this->faker->name,
    //         'email' => $this->faker->unique()->safeEmail,
    //         'password' => 'testing123',
    //         'password_confirmation' => 'testing123',
    //     ]);
    //
    //     $user = User::latest()->get();
    //
    //     $response->assertRedirect('/home');
    //
    //     $this->assertAuthenticatedAs($user);
    // }
    // public function testContactForm()
    // {
    //     $response = $this->post('/contact-form', [
    //         'name' => 'Fredrick James Cua',
    //         'email' => 'fredrickjamescua@gmail.com',
    //         'phone' => '09758114137',
    //         'subject' => 'Test',
    //         'message' => 'This is a message.',
    //     ]);
    //     dd($response);
    //
    //     $response->assertStatus(200);
    // }
    /**
     * A basic feature test example.
     *
     * @return void
     */
     public function user_can_contact_admin()
     {
         $response = $this->post('/contact-form', [
             'name' => 'Fredrick James Cua',
             'email' => 'fredrickjamescua@gmail.com',
             'phone' => '09758114137',
             'subject' => 'Test',
             'message' => 'This is a message.',
         ]);
         $response->assertStatus(201);
     }
    public function user_can_update_his_profile()
    {
        $response = $this->post('/profile', [
            'name' => 'Fredrick James Cua',
            'email' => 'fredrickjamescua@gmail.com',
            'phone' => '09758114137',
        ]);
        // dd($response);
        // $this->assertTrue(true);
        $response->assertStatus(201);
        // $this->assertTrue(count(Task::all()) > 1);
        // $user = User::first();

        // $attributes = [
        //     'name' => $this->faker->name,
        //     'email' => $this->faker->email,
        //     'phone' => $this->faker->phone,
        // ];
        //
        // $this->postJson('/profile', $attributes)
        //     ->assertStatus(200);
        //
        // // $this->assertDatabaseHas($user->getTable(), array_merge($attributes, [
        // //     'id' => $user->id
        // // ]));
        //
        // $this->markTestIncomplete();
    }

    // public function user_can_authenticate_his_account()
    // {
    //     $user = User::first();
    //     $this->assertAuthenticatedAs($user, $guard = 'web')
    //         ->assertStatus(200);;
    // }
}
