@extends('layouts.admin')

@section('title')
Contact
@endsection

@section('content')
<section class="content">
<h2>Hey !</h2> <br><br>

You received an email from : {{ $name }} <br><br>

User details: <br><br>

Name:  {{ $name }}<br>
Email:  {{ $email }}<br>
Phone:  {{ $phone }}<br>
Subject:  {{ $subject }}<br>
Message:  {{ $comment }}<br><br>

Thanks
@endsection
