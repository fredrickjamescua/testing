const axios = window.axios;

const BASE_API_URL = 'http://127.0.0.1:8000/api';

export default {
    getAllPosts: () => axios.get(`${BASE_API_URL}/posts`),
    getPost: (id) => axios.get(`${BASE_API_URL}/posts/${id}/edit`),
    createPost: (post) => axios.post(`${BASE_API_URL}/posts`, post),
    updatePost: (id, post) => axios.put(`${BASE_API_URL}/posts/${id}`, post),
    deletePost: (id) => axios.delete(`${BASE_API_URL}/posts/${id}`),
    registerUser: (post) => axios.post(`${BASE_API_URL}/auth/signup`, post),
}
