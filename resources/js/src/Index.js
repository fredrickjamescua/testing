import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import Main from './Router';

const Index = () => {
    return (
        <BrowserRouter>
          <Route>
            <Main />
          </Route>
        </BrowserRouter>
    );
};

ReactDOM.render(<Index />, document.getElementById('app'));
