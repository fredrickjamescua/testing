import React, {Component} from 'react';
import RegisterContainer from './RegisterContainer';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: props.location,
            isLoggedIn: false,
            user: {}
        }
    }

    componentWillMount() {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({ isLoggedIn: AppState.isLoggedIn, user: AppState.user });
        }
    }

    render() {
        return (
            <div className="content">
                <Header userData={this.state.user} userIsLoggedIn={this.state.isLoggedIn} />
                <RegisterContainer redirect={this.state.redirect} />
                <Footer />
            </div>
        );
    }
}

export default Register
